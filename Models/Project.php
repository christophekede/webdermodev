<?php
class Project {

function getProject($id){
  $db=Model::get_model();
  $db=$db->getDB();

  $query="select * from project where idprojet=$id";
  $req= $db->prepare($query);
  $req->execute();

  return $req->fetchAll(PDO::FETCH_ASSOC);


}


function getAllProject(){
  $db=Model::get_model();
  $db=$db->getDB();

  $query="select * from project";
  $req= $db->prepare($query);
  $req->execute();

  return $req->fetchAll(PDO::FETCH_ASSOC);


}

function getImagesProject($idprojet){
  $db=Model::get_model();
  $db=$db->getDB();

  $query="select link from projectimages join project using (idprojet)  ";
  $req= $db->prepare($query);
  $req->execute();

  return $req->fetchAll(PDO::FETCH_ASSOC);


}


function getProjectByCategory($name){
  $db=Model::get_model();
  $db=$db->getDB();

  $query="select * from projectcategory join project using(idprojet) where idcategory=(select idcategory from category where name=:name)";
  $req= $db->prepare($query);
  $req->bindValue(':name', $name);
  $req->execute();

  return $req->fetchAll(PDO::FETCH_ASSOC);


}




}