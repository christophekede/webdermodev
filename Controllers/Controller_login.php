<?php

class Controller_login extends Controller{

    public function action_login(){
        
        $m = Model::get_model(); // On récupère le modèle
        $data = [
            //"nb" => $m->get_nb_nobel_prizes() //On récupère le nombre de prix nobels
        ];
        $this->render("login",$data);
    }

    public function action_default(){
        $this->action_login();
    }
}