<?php

class Controller_home extends Controller{

    public function action_home(){
        
        $m = Model::get_model(); // On récupère le modèle
        $data = [
            //"nb" => $m->get_nb_nobel_prizes() //On récupère le nombre de prix nobels
        ];
        $this->render("home",$data);
    }

    public function action_default(){
        $this->action_home();
    }
}