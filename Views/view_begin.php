<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Cosmo'Starter</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

		<link rel="stylesheet" href="Content/css/font.css">
		<link rel="stylesheet" href="Content/css/navbar.css">

	</head>
	<body>
		<div class="row topnav">
			<div class="col-sm-4" ">
				<a href="#about" style="margin-left: 10%;font-size: 25px;">Co-Créons</a>
			</div>
			<div class="col-sm-4" >
				<a href="?controller=home" ><img src="Content/img/LOGO-DDP.png" width="20%" /></a>
			</div>
			<div class="col-sm-4" >
				<a href="#about" style="float: right"><span class="glyphicon glyphicon-menu-hamburger"></span></a>
				<a href="#contact" style="float: right"><span class="glyphicon glyphicon-user"></span> Sign Up</a>
				<a href="?controller=login" style="float: right"><span class="glyphicon glyphicon-log-in"></span> Login</a>
				
			</div>
		</div>