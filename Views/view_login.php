<?php require("view_begin.php"); ?>


		<div class="Project">
			<div class="titre">
				<h1>Connexion</h1>
			</div>
			<form action="action_page.php">
				<div class="container">
					<label for="uname"><b>Adresse mail:</b></label>
					<input type="text" placeholder="Enter Username" name="uname" required>
					<br>
					<br>
					<label for="psw" ><b>Mot de passe:</b></label>
					<input type="password" placeholder="Enter Password" name="psw" required>
					<br>
					<br>
					<button type="submit">Login</button>
					<br>
					<label>
						<input type="checkbox" checked="checked" name="remember"> Remember me
					</label>
				</div>

				<div class="container">
					<button type="button" class="cancelbtn">Annuler</button>
					<br>
					<span class="psw"><a href="#">Mot de passe oublié?</a></span>
					<br>
					<span class="psw">Pas de compte? <a href="#">Créer un compte.</a></span>
			
				</div>
			</form>
					
		</div>
		
	</body>
</html>
